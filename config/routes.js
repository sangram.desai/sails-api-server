/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

    'post /signup': {
        action: 'user/create',
        cors: false,
        // cors: {
        //     allowOrigins: ['http://example.com', 'https://api.example.com', 'http://blog.example.com:1337', 'https://foo.com:8888'],
        //     allowCredentials: false
        // }
    },
    'post /product':{
        action: 'product/uploadPost',
    },
    'put /product':{
        action:"product/uploadPut"
    },
    'post /department':{
        action: 'department/uploadPost',
    },
    'put /department':{
        action:"department/uploadPut"
    },
    'get /department/:departmentId/product':{
        action:"product/productInDept"
    }

};
