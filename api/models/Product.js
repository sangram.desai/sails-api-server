/**
 * Product.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    productName: {
      type: "string",
      maxLength: 200
    },
    productCode: {
      type: "string",
      maxLength: 200,
      allowNull: false
    },
    price: {
      type: "number",
    },
    isNew: {
      type: "boolean"
    },
    productImage: {
      type: "string",
      maxLength: 500,
    },
    department: {
      model: 'department',
      required: true
    }
  },

};

