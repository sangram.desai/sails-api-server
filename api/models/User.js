/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var bcrypt = require("bcrypt");
var Promise = require("bluebird");

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    firstName:{
      type: "string",
      maxLength:50
    },
    lastName:{
      type: "string",
      maxLength:50
    },
    address1:{
      type:"string",
      maxLength:300
    },
    address2:{
      type:"string",
      maxLength:300
    },
    landmark:{
      type:"string",
      maxLength:300
    },
    email: {
      type: "string",
      required: true,
      unique: true
    },
    password:{
      type: "string",
      minLength: 6,
      
      required: true,
      columnName: "encryptedPassword"
    },  
    city: {
      type: "string",
      maxLength:300
    },
    zipCode: {
      type: "string",
      maxLength:20
    },
  },
  customToJSON: function() {
    return _.omit(this, ['password'])
  },
  beforeCreate: function(values, cb){
		bcrypt.hash(values.password, 10, function (err, hash) {
      if (err) return cb(err);
      values.password = hash;
      cb();
    });
  },
  comparePassword: function(password, user) {
		return new Promise(function (resolve, reject) {
      bcrypt.compare(password, user.password, function (err, match) {
        if (err) reject(err);

        if (match) {
          resolve(true);
        } else {
          reject(err);
        }
      })
    });
	}

};

