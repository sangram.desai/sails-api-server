/**
 * Department.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    departmentName: {
      type: "string",
      maxLength: 200
    },
    departmentImage: {
      type: "string",
      maxLength: 500,
    },
    product: {
      required: false,
      collection: 'product',
      via: 'department'
    }

  },

};

