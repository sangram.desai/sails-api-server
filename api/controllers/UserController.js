var _ = require('lodash');

module.exports = {
  _config: {
    actions: false,
    shortcuts: false,
    rest: false
  },
  create: function (req, res) {
    if (req.body.password !== req.body.confirmPassword) {
      return ResponseService.json(401, res, "Password doesn't match")
    }

    var allowedParameters = [
      "email", "password","firstName","lastName","address1","address2","landmark"
    ]

    var data = _.pick(req.body, allowedParameters);

    User.create(data).fetch().then(function (user) {
      var responseData = {
        user: user,
        token: JwtService.issue({id: user.id})
      }
      return ResponseService.json(200, res, "User created successfully", responseData)
    }).catch(function (error) {
          return ResponseService.json(400, res, "User could not be created", error.Errors)
      }
    )

  }
};
