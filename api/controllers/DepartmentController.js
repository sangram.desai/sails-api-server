/**
 * DepartmentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var path = require('path');

module.exports = {
  _config: {
    actions: false,
    shortcuts: false,
    rest: true
  },
  uploadPost: function (req, res) {

     console.log("req.body.departmentName",req.body.departmentName);
     console.log("req.file('departmentImage')",req.file('departmentImage'));

     var uploadFile = req.file('departmentImage');

    uploadFile.upload({ dirname: "../../assets/images" }, function onUploadComplete(err, files) {

      if (err){
        console.log("File Upload Error:",err);
        return res.serverError(err);
      }
      else {
        console.log(files);

        if (files.length > 0) {
          var filename = path.basename(files[0].fd);

          var data = {
            "departmentName": req.body.departmentName,
            "departmentImage": filename
          }

          Department.create(data).fetch().then(function (product) {
            return ResponseService.json(200, res, "Department created successfully", product)
          }).catch(function (error) {
            console.log("Department could not be created");
            return ResponseService.json(400, res, "Department could not be created", error.Errors)
          });
        } else {
          console.log("Department Image could not be uploaded");
          return ResponseService.json(400, res, "Department Image could not be uploaded", null)
        }




      }

    });
  },
  uploadPut: function (req, res) {
    var uploadFile = req.file('departmentImage');
    uploadFile.upload({ dirname: "../../assets/images" }, function onUploadComplete(err, files) {

      if (err)
        return res.serverError(err);
      else {
        console.log(files);
        var filename = path.basename(files[0].fd);
        var departmentId = req.body.departmentId;

        var data = {
          "departmentName": req.body.departmentName,
          "departmentImage": filename
        }

        var updatedRecord = Department.updateOne({ "id": departmentId })
          .set(data)
          .then(function (department) {
            console.log(department);
            return ResponseService.json(200, res, "Department updated successfully", department)
          }).catch(function (error) {
            return ResponseService.json(400, res, "Department could not be updated", error.Errors)
          });


      }

    });
  },
};

