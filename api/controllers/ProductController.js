/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var path = require('path');

module.exports = {
  _config: {
    actions: false,
    shortcuts: false,
    rest: true
  },

  uploadPost: function (req, res) {
    var uploadFile = req.file('productImage');
    uploadFile.upload({ dirname: "../../assets/images" }, function onUploadComplete(err, files) {

      if (err) 
        return res.serverError(err);
      else {
        console.log(files);
        var filename = path.basename(files[0].fd);

        var data = {
          "productName": req.body.productName,
          "productImage": filename,
          "price": req.body.price,
          "isNew": req.body.isNew,
          "productCode": req.body.productCode,
          "department": req.body.department
        }

        Product.create(data).fetch().then(function (product) {
          return ResponseService.json(200, res, "Product created successfully", product)
        }).catch(function (error) {
          return ResponseService.json(400, res, "Product could not be created", error.Errors)
        });
      }

    });
  },

  uploadPut: function (req, res) {
    var uploadFile = req.file('productImage');
    uploadFile.upload({ dirname: "../../assets/images" }, function onUploadComplete(err, files) {

      if (err) 
        return res.serverError(err);
      else {
        console.log(files);
        var filename = path.basename(files[0].fd);
        var productId = req.body.productId;

        var data = {
          "productName": req.body.productName,
          "productImage": filename,
          "price": req.body.price,
          "isNew": req.body.isNew,
          "productCode": req.body.productCode,
          "department": req.body.department
        }

        var updatedRecord =  Product.updateOne({"id":productId})
        .set(data)
        .then(function(product){
          console.log(product);
          return ResponseService.json(200, res, "Product updated successfully", product)
        }).catch(function (error) {
          return ResponseService.json(400, res, "Product could not be updated", error.Errors)
        });


      }

    });
  },

  productInDept:async function(req,res){
    let deptId = req.param.departmentId;
    let product = await Product.find({"department":deptId})
    //console.log(product);
    return ResponseService.json(200, res, "Product in department", product)
  }

};

